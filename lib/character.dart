import 'package:flutter/material.dart';
import 'package:gwammcalc_flutter/screens/title_selection.dart';

class Character extends StatelessWidget {
  final String characterName;
  final Color professionColor;
  final IconData professionIcon;

  const Character({
    Key key,
    @required this.characterName,
    @required this.professionColor,
    @required this.professionIcon,
  })  : assert(characterName != null),
        assert(professionColor != null),
        assert(professionIcon != null),
        super(key: key);

  void onTapped(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => TitleSelectionScreen(
            characterName: characterName, professionColor: professionColor),
      ),
    );
  }

  Widget build(BuildContext context) {
    return Container(
      height: 100,
      color: professionColor,
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (BuildContext context) => new TitleSelectionScreen(
                        characterName: characterName,
                        professionColor: professionColor,
                      ),
                ));
          },
          borderRadius: BorderRadius.circular(50),
          child: Row(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.all(16.0),
                child: Icon(
                  professionIcon,
                  size: 60,
                ),
              ),
              new Center(
                child: Text(
                  characterName,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
