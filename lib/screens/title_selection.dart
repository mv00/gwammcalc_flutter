import 'package:flutter/material.dart';
import 'package:gwammcalc_flutter/title.dart';

class TitleSelectionScreen extends StatefulWidget {
  final String characterName;
  final Color professionColor;

  const TitleSelectionScreen({
    Key key,
    @required this.characterName,
    @required this.professionColor,
  })  : assert(characterName != null),
        assert(professionColor != null),
        super(key: key);

  @override
  _TitleSelectionScreenState createState() => _TitleSelectionScreenState();
}

class _TitleSelectionScreenState extends State<TitleSelectionScreen> {
  final _titles = _getTitles();

  final _achievedTitles = new Set<GwTitle>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          _achievedTitles.length.toString() + '  ' + 'Titles',
        ),
      ),
      body: _buildTitleList(),
    );
  }

  Widget _buildTitleList() {
    return new ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemCount: _titles.length * 2,
      itemBuilder: (context, i) {
        if (i.isOdd) return new Divider();

        final index = i ~/ 2;

        return _buildRow(_titles[index]);
      },
    );
  }

  Widget _buildRow(GwTitle title) {
    final alreadySaved = _achievedTitles.contains(title);
    return new ListTile(
      title: new Text(title.name),
      trailing: alreadySaved
          ? new Tab(
                  icon: new Image.asset("lib/image/250px-Hard_Mode_Helmet.png",
                      scale: 9))
              .icon
          : new Tab(
                  icon: new Image.asset(
                      "lib/image/250px-Hard_Mode_Helmet-grey.png",
                      scale: 9))
              .icon,
      onTap: () {
        setState(
          () {
            if (alreadySaved) {
              _achievedTitles.remove(title);
            } else {
              
              _achievedTitles.add(title);
            }
          },
        );
      },
    );
  }

  static List<GwTitle> _getTitles() {
    var legendaryGuardian = [
      GwSingleTitle(id: 1, name: "Protector of Tyria"),
      GwSingleTitle(id: 2, name: "Protector of Cantha"),
      GwSingleTitle(id: 3, name: "Protector of Elona"),
      GwSingleTitle(id: 4, name: "Guardian of Tyria"),
      GwSingleTitle(id: 5, name: "Guardian of Cantha"),
      GwSingleTitle(id: 6, name: "Guardian of Elona"),
    ];

    var legendaryCartographer = [
      GwSingleTitle(id: 7, name: "Tyrian Grandmaster Cartographer"),
      GwSingleTitle(id: 8, name: "Canthan Grandmaster Cartographer"),
      GwSingleTitle(id: 9, name: "Elonian Grandmaster Cartographer"),
    ];

    var legendaryVanquisher = [
      GwSingleTitle(id: 10, name: "Tyrian Vanquisher"),
      GwSingleTitle(id: 11, name: "Canthan Vanquisher"),
      GwSingleTitle(id: 12, name: "Elonian Vanquisher")
    ];

    var legendarySkillhunter = [
      GwSingleTitle(id: 13, name: "Tyrian Elite Skill Hunter"),
      GwSingleTitle(id: 14, name: "Canthan Elite Skill Hunter	"),
      GwSingleTitle(id: 15, name: "Elonian Elite Skill Hunter	")
    ];

    return [
      GwMultiTitle(
          id: 16,
          name: "Legendary Guardian",
          enclosingTitles: legendaryGuardian),
      GwMultiTitle(
          id: 17,
          name: "Legendary Cartographer",
          enclosingTitles: legendaryCartographer),
      GwMultiTitle(
          id: 18,
          name: "Legendary Vanquisher",
          enclosingTitles: legendaryVanquisher),
      GwMultiTitle(
          id: 19,
          name: "Legendary Skillhunter",
          enclosingTitles: legendarySkillhunter),
      GwSingleTitle(id: 20, name: "Incorrigible Ale-Hound"),
      GwSingleTitle(id: 21, name: "Life of the Party"),
      GwSingleTitle(id: 22, name: "Connoisseur of Confectionaries"),
      GwSingleTitle(id: 23, name: "Legendary Survivor"),
      GwSingleTitle(id: 24, name: "Legendary Defender of Ascalon"),
      GwSingleTitle(id: 25, name: "Holy Lightbringer"),
      GwSingleTitle(id: 26, name: "Legendary Spearmarshal"),
      GwSingleTitle(id: 27, name: "Asura"),
      GwSingleTitle(id: 28, name: "Norn"),
      GwSingleTitle(id: 29, name: "Deldrimor"),
      GwSingleTitle(id: 30, name: "Ebon Vanguard"),
      GwSingleTitle(id: 31, name: "Master of the North"),
      GwSingleTitle(id: 32, name: "Blessed by Fate"),
      GwSingleTitle(id: 33, name: "Cursed by Fate"),
      GwSingleTitle(id: 34, name: "Grandmaster Treasure Hunter"),
      GwSingleTitle(id: 35, name: "Source of Wisdom"),
      GwSingleTitle(id: 36, name: "Legendary Hero of the Zaishen"),
      GwSingleTitle(id: 37, name: "Savior of the Kurzicks"),
      GwSingleTitle(id: 38, name: "Savior of the Luxons"),
      GwSingleTitle(id: 39, name: "Legendary Champion"),
      GwSingleTitle(id: 40, name: "Codex Grandmaster"),
      GwSingleTitle(id: 41, name: "Real Ultimate Power Skillz"),
      GwSingleTitle(id: 42, name: "Legendary Gladiator"),
      GwSingleTitle(id: 43, name: "Legendary Hero"),
    ];
  }
}
