import 'package:flutter/material.dart';
import 'package:gwammcalc_flutter/screens/add_character.dart';
import 'package:gwammcalc_flutter/screens/character.dart';
import 'package:gwammcalc_flutter/screens/character_selection.dart';
import 'package:gwammcalc_flutter/screens/home.dart';

void main() {
  runApp(new MyApp());
}

String characterName;
ColorSwatch professionColor;
IconData professionIcon;

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'GWAMM Calc',
      home: HomeScreen(
        title: 'GWAMM Calc',
        backgroundcolor: Colors.blue,
        splashcolor: Colors.blue,
      ),
      routes: <String, WidgetBuilder>{
        '/character_selection': (BuildContext context) =>
            new CharacterSelectionScreen(),
        '/character': (BuildContext context) => new CharacterScreen(
              characterName: characterName,
              professionColor: professionColor,
              professionIcon: professionIcon,
            ),
        '/add_character': (BuildContext context) => new AddCharacterScreen(),
      },
    );
  }
}
