import 'package:flutter/material.dart';

class AddCharacterScreen extends StatefulWidget {
  @override
  _AddCharacterScreenState createState() => _AddCharacterScreenState();
}

class _AddCharacterScreenState extends State<AddCharacterScreen> {
  String characterName;
  String profession;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Character'),
      ),
      body: new Center(
        child: new Column(
          children: [
            new TextFormField(
              decoration: new InputDecoration(labelText: 'Character name'),
              validator: (val) =>
                  val.isEmpty ? "Please enter your character's" : null,
              onSaved: (val) => characterName = val,
            ),
            new TextFormField(
              decoration: new InputDecoration(labelText: 'Profession'),
              validator: (val) =>
                  val.isEmpty ? 'Please choose profession' : null,
              onSaved: (val) => profession = val,
              obscureText: true,
            ),
            new Padding(
              padding: new EdgeInsets.only(top: 20.0),
              child: new RaisedButton(
                  onPressed: () {},
                  child: new Container(
                    child: Row(
                      children: <Widget>[
                        new Icon(Icons.done),
                        new Text('Submit')
                      ],
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
