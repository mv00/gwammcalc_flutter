import 'package:flutter/material.dart';
import 'package:gwammcalc_flutter/character.dart';

class CharacterSelectionScreen extends StatelessWidget {
  final _characters = _loadCharacter();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Character Selection'),
      ),
      body: _buildCharacterList(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).pushNamed('/add_character'),
        child: Text('+'),
      ),
    );
  }

  Widget _buildCharacterList() {
    return new ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemCount: _characters.length * 2,
      itemBuilder: (context, i) {
        if (i.isOdd) return new Divider();
        final index = i ~/ 2;
        return _characters[index];
      },
    );
  }

  static List<Character> _loadCharacter() {
    return List.from([
      Character(
          characterName: "O R O K I N",
          professionColor: Color.fromARGB(255, 255, 200, 0),
          professionIcon: Icons.center_focus_strong),
      Character(
          characterName: "Keyboard Frenzy",
          professionColor: Colors.purple,
          professionIcon: Icons.hourglass_empty)
    ]);
  }
}
