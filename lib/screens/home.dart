import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  final String title;
  final Color backgroundcolor;
  final Color splashcolor;

  const HomeScreen({
    Key key,
    @required this.title,
    @required final this.backgroundcolor,
    @required final this.splashcolor,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: backgroundcolor,
        actions: <Widget>[
          new IconButton(
            icon: Icon(Icons.people),
            iconSize: 40,
            splashColor: splashcolor,
            onPressed: () =>
                Navigator.of(context).pushNamed('/character_selection'),
          ),
        ],
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.home, size: 140),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.help),
                  Text('I am some information describing how and why'),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
