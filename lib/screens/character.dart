import 'package:flutter/material.dart';

class CharacterScreen extends StatelessWidget {
  final String characterName;
  final Color professionColor;
  final IconData professionIcon;

  const CharacterScreen({
    Key key,
    @required this.characterName,
    @required final this.professionColor,
    @required final this.professionIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(characterName),
        backgroundColor: professionColor,
      ),
      body: Container(
        height: 50,
        color: professionColor,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () {},
            child: Row(
              children: <Widget>[
                new Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(
                    professionIcon,
                    size: 60,
                  ),
                ),
                new Center(
                  child: Text(
                    characterName,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
