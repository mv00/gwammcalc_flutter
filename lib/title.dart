class GwTitle {
  int id;
  String name;
}

class GwSingleTitle extends GwTitle {
  int id;
  String name;

  GwSingleTitle({
    this.id,
    this.name,
  })  : assert(id != null),
        assert(name != null),
        super();
}

class GwMultiTitle extends GwTitle {
  int id;
  String name;
  List<GwSingleTitle> enclosingTitles;

  GwMultiTitle({this.id, this.name, this.enclosingTitles})
      : assert(id != null),
        assert(name != null),
        assert(enclosingTitles != null && enclosingTitles.length > 0),
        super();
}
